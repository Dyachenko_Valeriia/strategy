package main;

public class WifiStrategy implements Strategy{
    @Override
    public String connectInternet() {
        return "Подключение через Wifi";
    }
}
