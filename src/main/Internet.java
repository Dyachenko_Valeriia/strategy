package main;

public class Internet {
    private Strategy strategy;

    public Internet(Strategy strategy) {
        this.strategy = strategy;
    }

    public String connectInternet() {
        return strategy.connectInternet();
    }

    public String off() {
        return "Подключение выключено";
    }

}
