package test;

import main.Dish;
import main.Internet;
import main.WiFi;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestStrategy {
    @Test
    public void testDish(){
        Internet dish = new Dish();
        assertEquals("Подключение через тарелку", dish.connectInternet());
    }

    @Test
    public void testWifi(){
        Internet wifi = new WiFi();
        assertEquals("Подключение через Wifi", wifi.connectInternet());
    }
}
